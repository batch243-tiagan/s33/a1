fetch("https://jsonplaceholder.typicode.com/todos")
	.then((response) => response.json())
	.then((json) => console.log(json));

//===================================================================
	const listArray = [];
	fetch("https://jsonplaceholder.typicode.com/todos")
	.then((response) => response.json())
	.then((json) => {
		json.map((elem) => {
			listArray.push(elem.title);
		});
	});
	console.log(listArray);

//===================================================================

	fetch("https://jsonplaceholder.typicode.com/todos/1")
	.then((response) => response.json())
	.then((json) => console.log(json));

//===================================================================

	fetch("https://jsonplaceholder.typicode.com/todos/1")
	.then((response) => response.json())
	.then((json) => console.log(`The item ${json.title} on the list has a status of ${json.completed}`));

//===================================================================


	fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
			'Content-type': 'application/json',
		},
		body: JSON.stringify({
			completed: false,
			id: 201,
			title: "Created To Do List Item",
			userId: 1
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json));

//===================================================================
	
	async function fetchData(){
	fetch('https://jsonplaceholder.typicode.com/todos/1', {
		method: 'PUT',
		headers: {
	  	'Content-type': 'application/json',
		},
		body: JSON.stringify({
			dateCompleted: "Pending",
			description: "To update the my to do list with a different data structure",
			id: 1,
			status: "Pending",
			title: "Updated To Do List Item",
			userId: 1
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json));
	}
	fetchData();

	//===================================================================

	fetch('https://jsonplaceholder.typicode.com/todos/1', {
		method: 'PUT',
		headers: {
	  	'Content-type': 'application/json',
		},
		body: JSON.stringify({
			completed: false,
			dateCompleted: "07/09/21",
			id: 1,
			status: "Completed",
			title: "delectus aut autem",
			userId: 1
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json));

	//===================================================================

	fetch('https://jsonplaceholder.typicode.com/todos/1', {
		method: 'PATCH',
		body: JSON.stringify({
			completed: true,
			title:"This is a patched To do list item"
			}),
		headers: {
			'Content-type': 'application/json; charset=UTF-8'
			}
		})
	.then(response => response.json())
	.then(json => console.log(json));

	//===================================================================

	fetch('https://jsonplaceholder.typicode.com/todos/1', {
		method: 'PATCH',
		body: JSON.stringify({
			completed: true,
			dateCompleted: "07/09/21"
			}),
		headers: {
			'Content-type': 'application/json; charset=UTF-8'
			}
		})
	.then(response => response.json())
	.then(json => console.log(json));

	//===================================================================

	fetch('https://jsonplaceholder.typicode.com/todos/1', {
	  method: 'DELETE'
	});